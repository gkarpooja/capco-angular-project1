import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../model/employee';

@Injectable()
export class EmployeeService {

  employees: Employee[];
  /**
   * SAMPLE DATA URL
   */
  CONFIG_JSON_URL: string = './assets/sample_data.json';

  /**
   * 
   * @param httpRequest : HttpClient : Makes the http request for the available resources.
   */
  constructor(private httpRequest: HttpClient) { }

  /**
   * This method helps to load list of employee from the JSON object which would be of Observable
   */
  public loadEmployees() : Observable<Employee[]>{
    return this.httpRequest.get<Employee[]>(this.CONFIG_JSON_URL);
  }

  /**
   *  This method helps to update the data for the passed employee whose id matches in the storage
   * 
   * @param id : Employee id 
   * @param status : Employee status
   */
  public updateEmploye(id: number, status: string,) {
    this.employees = this.getEmployeesFromStorage();
    const index = this.employees.findIndex(e => e.id === id); 
    this.employees.splice(index, 1); 
    console.log('Updated list : ' + this.employees)
  }

  /**
   *  This method helps to store all the Employee object in the browsers session storage.
   * 
   * @param employees : List of employee object to be stored
   */
  public storeEmployeeToStorage(employees: Employee[]) {
    sessionStorage.setItem("key_employee", JSON.stringify(employees));
  }

  /**
   * This method helps to retrieve the Employees based on employee id.
   * 
   * @returns : Returns an Employee whose id matches
   */
  public getEmployeesById(id: number): Employee {
    let employee: Employee ;
    this.employees = this.getEmployeesFromStorage();
    this.employees.forEach( emp => {
        if( emp.id === id ) {
          employee = emp;
        } else{
          employee = null;
        }
      });
      return employee;
  }

  /**
   * This method helps to retrieve the Stored Employees from the Session Storage.
   * 
   * @returns : Returns an array of Employee
   */
  public getEmployeesFromStorage(): any[] {
    return JSON.parse(sessionStorage.getItem("key_employee"));
  }

  /**
   * This method is used to submit the employee id along with its status
   * @param employee : contains the whole employee data
   */
  submitEmployeeStatus(employee) {
    this.httpRequest.post('/api/submit'+ employee.id, employee.status);
  }

  /**
   * 
   * @param error Handles the error in generic way
   */
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
  }
}