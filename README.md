## Project Description 

This is Angular project with version 5.x which has the following implementation points :

1. The component should display Sample Data in a table
2. User should be able to select how many rows are displayed in the table
3. Table should be paginated if not all rows are displayed on the screen based on the user�s selection
4. Pagination options should be displayed in the table footer
5. Column names should be displayed in the table header
6. Entire table, table header and table footer should always be displayed on the screen while scrolling
7. If number of rows exceeds the size of the table body, a vertical scrollbar should be displayed within the table body � only table body shall scroll vertically, table header and footer shall remain as is
8. If number of columns exceed the size of the table body, a horizontal scrollbar should be displayed within the table    body � only table body and table header shall scroll to reveal the additional columns, table footer shall remain as is
9. Each row should contain a button which shall submit the row ID and row status to /api/submit as a POST request � You are not expected to create the POST endpoint, but you can mock one if you like

---

## Technologies:

. Angular 5+
. Angular CLI
. Karma / Jasmine

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

---

# CapcoAngularTask

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).